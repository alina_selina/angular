# Projects App #

### Web API project ###

Download [Web API project](https://bitbucket.org/alina_selina/ef.git)

### Web Client project ###

How to run:

* npm install
* ng serve

### Notes ###

Не везде используется каскадное удаление, поэтому user'ов, на которых есть ссылки нельзя удалить, уже исправляю это.