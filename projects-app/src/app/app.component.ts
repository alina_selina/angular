import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'ProjectManager';
  navigation: { link: string, title: string }[] = [
    { link: '/users', title: 'Users' },
    { link: '/teams', title: 'Teams' },
    { link: '/projects', title: 'Projects' },
    { link: '/tasks', title: 'Tasks' }
  ];
}
