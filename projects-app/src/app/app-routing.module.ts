import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamListComponent } from './modules/teams/components/team-list/team-list.component';
import { TeamDetailsComponent } from './modules/teams/components/team-details/team-details.component';
import { TeamCreateComponent } from './modules/teams/components/team-create/team-create.component';
import { UserListComponent } from './modules/users/components/user-list/user-list.component';
import { UserDetailsComponent } from './modules/users/components/user-details/user-details.component';
import { TaskListComponent } from './modules/tasks/components/task-list/task-list.component';
import { TaskDetailsComponent } from './modules/tasks/components/task-details/task-details.component';
import { ProjectListComponent } from './modules/projects/components/project-list/project-list.component';
import { ProjectDetailsComponent } from './modules/projects/components/project-details/project-details.component';
import { ExitGuard } from './modules/shared/guards/exit.guard';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ErrorPageComponent } from './modules/shared/components/error-page/error-page.component';

const routes: Routes = [
  {
    path: 'teams',
    component: TeamListComponent
  }, {
    path: 'teams/create',
    component: TeamCreateComponent,
    canDeactivate: [ExitGuard]
  }, {
    path: 'teams/:id',
    component: TeamDetailsComponent
  }, {
    path: 'users',
    component: UserListComponent
  }, {
    path: 'users/:id',
    component: UserDetailsComponent
  }, {
    path: 'tasks',
    component: TaskListComponent
  }, {
    path: 'tasks/:id',
    component: TaskDetailsComponent
  }, {
    path: 'projects',
    component: ProjectListComponent
  }, {
    path: 'projects/:id',
    component: ProjectDetailsComponent
  }, {
    path: 'error', 
    component: ErrorPageComponent
  }, {
    path: '',
    component: HomePageComponent
  }, {
    path: '**', 
    component: ErrorPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
