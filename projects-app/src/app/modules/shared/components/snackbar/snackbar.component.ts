import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.css']
})
export class SnackbarComponent implements OnInit {
  public isShowSnackbar:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  public showSnack(message: string) {
    let snackbar: HTMLElement = <HTMLElement>document.getElementById("snackbar");
    snackbar.innerText = message;
    this.isShowSnackbar=true;

    setTimeout(()=>this.isShowSnackbar=false, 3000);
  }

}
