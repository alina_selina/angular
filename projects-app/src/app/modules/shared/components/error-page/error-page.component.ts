import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent {
  errorCode: number;
  errorText: string;

  constructor(private route: Router) {
    this.errorCode = this.route.getCurrentNavigation()?.extras?.state?.error ?? 404;
    this.errorText = this.route.getCurrentNavigation()?.extras?.state?.message ?? "Not Found";
  }

}
