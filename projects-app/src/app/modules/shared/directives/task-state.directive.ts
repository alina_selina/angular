import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[taskState]'
})
export class TaskStateDirective {
  @Input() bcolor: string;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  ngAfterViewInit(): void {
    let element: HTMLElement = this.elementRef.nativeElement;

    this.renderer.setStyle(element, "background-color", this.bcolor);
    this.renderer.setStyle(element, "border", "solid 1px lightgray");
    this.renderer.setStyle(element, "padding-left", "2px");
    this.renderer.setStyle(element, "padding-right", "2px");
    this.renderer.setStyle(element, "border-radius", "8px");
  }
}
