import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  public get<T>(path: string, params: HttpParams = new HttpParams()): Observable<T[] | T> {
    const url = `${environment.apiUrl}${path}`;

    return this.httpClient.get<T[] | T>(url, { params })
      .pipe(retry(2), catchError(this.handleError));
  }

  public put<T>(path: string, body: Object = {}): Observable<T> {
    const url = `${environment.apiUrl}${path}`;

    return this.httpClient.put<T>(url, body, httpOptions)
      .pipe(catchError(this.handleError));
  }

  public post<T>(path: string, body: Object = {}): Observable<T> {
    const url = `${environment.apiUrl}${path}`;

    return this.httpClient.post<T>(url, body, httpOptions)
      .pipe(catchError(this.handleError));
  }

  public delete<T>(path: string): Observable<T> {
    const url = `${environment.apiUrl}${path}`;

    return this.httpClient.delete<T>(url, httpOptions)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }
}
