export enum TaskState {
    ToDo,
    InProgress,
    Done,
    Canceled
}
