export interface Team {
    id:number;
    name:string;
    createdAt:Date;
}

export interface CreateTeam {
    name:string;
    createdAt?:Date;
}
