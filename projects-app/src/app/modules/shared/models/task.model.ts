import { TaskState } from "../enums/task-state.model"

export interface Task {
    id: number;
    projectId: number;
    performerId: number;
    name: string;
    description: string;
    state: TaskState;
    createdAt: Date;
    finishedAt?: Date;
}
