import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatDatePipe } from './pipes/format-date.pipe';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './services';
import { SnackbarComponent } from './components/snackbar/snackbar.component';
import { ExitGuard } from './guards/exit.guard';
import { TaskStateDirective } from './directives/task-state.directive';
import { ErrorPageComponent } from './components/error-page/error-page.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [
    FormatDatePipe,
    SnackbarComponent,
    TaskStateDirective,
    ErrorPageComponent
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    FormatDatePipe,
    SnackbarComponent,
    TaskStateDirective
  ],
  providers: [
    ApiService,
    ExitGuard
  ]
})
export class SharedModule { }
