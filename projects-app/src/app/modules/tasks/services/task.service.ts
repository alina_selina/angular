import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../../shared/services';
import { Task } from '../../shared/models';

@Injectable({
  providedIn: 'root'
})

export class TaskService {
  private readonly url: string = 'tasks';

  constructor(private _apiService: ApiService) { }

  public getTasks(): Observable<Task[]> {
    return <Observable<Task[]>>this._apiService.get<Task>(this.url);
  }

  public getTask(id: number): Observable<Task> {
    return <Observable<Task>>this._apiService.get<Task>(`${this.url}/${id}`);
  }

  public deleteTask(id: number): Observable<Task> {
    return this._apiService.delete<Task>(`${this.url}/${id}`);
  }

  public updateTask(task: Task): Observable<Task> {
    return this._apiService.put<Task>(this.url, task);
  }
}
