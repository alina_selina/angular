import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaskService } from '../../services/task.service';
import { Task } from '../../../shared/models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateTaskModalComponent } from '../update-task-modal/update-task-modal.component';
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {
  @ViewChild(SnackbarComponent, { static: false })
  private snackbarComponent: SnackbarComponent | undefined;
  task: Task;
  title: string = "Task Info";

  constructor(private route: ActivatedRoute, private service: TaskService,
    private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.service.getTask(this.route.snapshot.params['id'])
      .subscribe(result => this.task = result,
        error => {
          console.log(error);
          this.router.navigate(['/error'], { state: { error: error.status, message: error.statusText } });
        });
  }

  openModal() {
    const modalRef = this.modalService.open(UpdateTaskModalComponent);
    modalRef.componentInstance.task = this.task;
    modalRef.result.then((result) => {
      if (result) {
        this.router.navigate(['/tasks'])
      } else {
        this.snackbarComponent?.showSnack("An error occured.");
      }
    });
  }

  delete() {
    if (confirm("Are you sure you want to delete this task?")) {
      this.service.deleteTask(this.task.id)
        .subscribe(() => this.router.navigate(['/tasks']),
          error => {
            this.snackbarComponent?.showSnack("An error occured.");
            console.log(error);
          });
    }
  }
}