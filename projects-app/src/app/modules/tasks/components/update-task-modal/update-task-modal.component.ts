import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TaskService } from '../../services/task.service';
import { Project, Task, User } from '../../../shared/models';
import { ProjectService } from '../../../projects/services/project.service';
import { UserService } from '../../../users/services/user.service';
import { TaskState } from 'src/app/modules/shared';

@Component({
  selector: 'app-update-task-modal',
  templateUrl: './update-task-modal.component.html',
  styleUrls: ['./update-task-modal.component.css']
})

export class UpdateTaskModalComponent implements OnInit {
  @Input() public task: Task;
  public taskForm: FormGroup;
  users: User[];
  projects: Project[];
  states: { key: number; text: string; }[] = [];
  title: string = "Task Info";

  constructor(private taskService: TaskService, private projectService: ProjectService,
    private userService: UserService, public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.taskForm = new FormGroup({
      'name': new FormControl(this.task.name,
        [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      'description': new FormControl(this.task.name,
        [Validators.required, Validators.minLength(3)]),
      'createdAt': new FormControl(this.task.createdAt, Validators.required),
      'finishedAt': new FormControl(this.task?.finishedAt),
      'projectId': new FormControl(this.task.projectId, Validators.required),
      'performerId': new FormControl(this.task.performerId, Validators.required),
      'state': new FormControl(this.task.state, Validators.required),
    });

    this.getUsers();
    this.getProjects();
    this.getStates();
  }

  getStates() {
    let i = 0;
    Object.keys(TaskState).filter(key => !Number(key) && key !== '0').forEach(key => {
      this.states.push({ key: i, text: key });
      i++;
    });
  }

  getUsers() {
    this.userService.getUsers()
      .subscribe(result => this.users = result,
        error => console.log(error));
  }

  getProjects() {
    this.projectService.getProjects()
      .subscribe(result => this.projects = result,
        error => console.log(error));
  }

  save() {
    this.taskService.updateTask(this.task)
      .subscribe(() => this.activeModal.close(this.task),
        () => this.activeModal.close());
  }
}
