import { Component, OnInit, ViewChild } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Task } from '../../../shared/models';
import { UpdateTaskModalComponent } from '../update-task-modal/update-task-modal.component';
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TaskState } from 'src/app/modules/shared';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  @ViewChild(SnackbarComponent, { static: false })
  private snackbarComponent: SnackbarComponent | undefined;
  title: string = "Project Tasks";
  states = TaskState;
  tasks: Task[] = [];

  constructor(private router:Router, private taskService: TaskService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getTasks();
  }

  getTasks(){
    this.taskService.getTasks()
    .subscribe(result => this.tasks = result,
      error=>{
        console.log(error);
        this.router.navigate(['/error'],{ state: { error: error.status, message: error.statusText }} );
      });
  }

  openModal(task: Task): void {
    const modalRef = this.modalService.open(UpdateTaskModalComponent);
    modalRef.componentInstance.task = task;
    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
      else {
        this.snackbarComponent?.showSnack("An error occured.");
      }
    });
  }

  delete(task: Task): void {
    if (confirm("Are you sure you want to delete this task?")) {
      this.taskService.deleteTask(task.id)
        .subscribe(() => this.getTasks(),
          error => {
            this.snackbarComponent?.showSnack("An error occured.");
            console.log(error);
          });
    }
  }
}
