import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskDetailsComponent } from './components/task-details/task-details.component';
import {SharedModule} from '../shared/shared.module';
import { TaskService } from './services/task.service';
import { AppRoutingModule } from '../../app-routing.module';
import { UpdateTaskModalComponent } from './components/update-task-modal/update-task-modal.component';



@NgModule({
  declarations: [
    TaskListComponent,
    TaskDetailsComponent,
    UpdateTaskModalComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [TaskService]
})
export class TasksModule { }
