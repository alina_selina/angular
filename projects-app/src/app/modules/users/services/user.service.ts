import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../../shared/services';
import { User } from '../../shared/models';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private readonly url: string = 'users';

  constructor(private _apiService: ApiService) { }

  public getUsers(): Observable<User[]> {
    return <Observable<User[]>>this._apiService.get<User>(this.url);
  }

  public getUser(id: number): Observable<User> {
    return <Observable<User>>this._apiService.get<User>(`${this.url}/${id}`);
  }

  public deleteUser(id: number): Observable<User> {
    return this._apiService.delete<User>(`${this.url}/${id}`);
  }

  public updateUser(user: User): Observable<User> {
    return this._apiService.put<User>(this.url, user);
  }
}

