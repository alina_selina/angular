import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../../shared/models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateUserModalComponent } from '../update-user-modal/update-user-modal.component';
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  @ViewChild(SnackbarComponent, { static: false })
  private snackbarComponent: SnackbarComponent | undefined;
  user: User;
  title:string="User Info";

  constructor(private route: ActivatedRoute, private service: UserService,
    private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.service.getUser(this.route.snapshot.params['id'])
      .subscribe(result => this.user = result,
        error=>{
          console.log(error);
          this.router.navigate(['/error'],{ state: { error: error.status, message: error.statusText }} );
        });
  }

  openModal() {
    const modalRef = this.modalService.open(UpdateUserModalComponent);
    modalRef.componentInstance.user = this.user;
    modalRef.result.then((result) => {
      if (result) {
        this.router.navigate(['/users'])
      } else {
        this.snackbarComponent?.showSnack("An error occured.");
      }
    });
  }

  delete() {
    if (confirm("Are you sure you want to delete this user?")) {
      this.service.deleteUser(this.user.id)
        .subscribe(() => this.router.navigate(['/users']),
          error => {
            this.snackbarComponent?.showSnack("An error occured.");
            console.log(error);
          });
    }
  }
}