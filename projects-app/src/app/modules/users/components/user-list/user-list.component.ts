import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../../shared/models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateUserModalComponent } from '../update-user-modal/update-user-modal.component';
import { SnackbarComponent } from 'src/app/modules/shared/components/snackbar/snackbar.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  @ViewChild(SnackbarComponent, {static: false})
  private snackbarComponent: SnackbarComponent|undefined;
  title:string="Users";
  users: User[] = [];

  constructor(private router: Router, private userService: UserService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.userService.getUsers()
      .subscribe(result => this.users = result,
        error=>{
          console.log(error);
          this.router.navigate(['/error'],{ state: { error: error.status, message: error.statusText }} );
        });
  }

  redirect(id: number) {
    this.router.navigate(['/users', id]);
  }

  openModal(user: User) {
    const modalRef = this.modalService.open(UpdateUserModalComponent);
    modalRef.componentInstance.user = user;
    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
      else{
        this.snackbarComponent?.showSnack("An error occured.");
      }
    });
  }

  delete(user: User) {
    if (confirm("Are you sure you want to delete this user?")) {
      this.userService.deleteUser(user.id)
        .subscribe(() => this.ngOnInit(),
          error => {
            this.snackbarComponent?.showSnack("An error occured.");
            console.log(error);
          });
    }
  }
}
