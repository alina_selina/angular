import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../services/user.service';
import { TeamService } from '../../../teams/services/team.service';
import { User, Team } from '../../../shared/models';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-update-user-modal',
  templateUrl: './update-user-modal.component.html',
  styleUrls: ['./update-user-modal.component.css']
})
export class UpdateUserModalComponent implements OnInit {
  @Input() public user:User;
  public teams:Team[];
  public userForm: FormGroup;
  title:string="User Info";

  constructor(private userService: UserService, private teamService:TeamService ,public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.userForm = new FormGroup({
      'firstName': new FormControl(this.user.firstName, 
        [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      'lastName': new FormControl(this.user.lastName, 
        [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      'email': new FormControl(this.user.email, 
        [Validators.required, Validators.email]),
      'birthDay': new FormControl(this.user.birthDay, Validators.required),
      'teamId': new FormControl(this.user.teamId, [Validators.min(1)]),
  });
  this.getTeams();
  }

  getTeams(){
    this.teamService.getTeams()
    .subscribe(result => {
      console.log(result);
      this.teams = result;
    }, error => {
      console.log(error);
    });
  }

  save() {
    this.userService.updateUser(this.user)
    .subscribe(() => this.activeModal.close(this.user), 
    () => this.activeModal.close());
  }
}