import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UserListComponent } from './components/user-list/user-list.component';
import {SharedModule} from '../shared/shared.module';
import { UserService } from './services/user.service';
import { AppRoutingModule } from '../../app-routing.module';
import { UpdateUserModalComponent } from './components/update-user-modal/update-user-modal.component';

@NgModule({
  declarations: [
    UserDetailsComponent,
    UserListComponent,
    UpdateUserModalComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [UserService]
})
export class UsersModule { }
