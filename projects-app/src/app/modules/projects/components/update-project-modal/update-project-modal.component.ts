import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project, Team, User } from '../../../shared/models';
import { ProjectService } from '../../../projects/services/project.service';
import { UserService } from '../../../users/services/user.service';
import { TeamService } from '../../../teams/services/team.service';

@Component({
  selector: 'app-update-project-modal',
  templateUrl: './update-project-modal.component.html',
  styleUrls: ['./update-project-modal.component.css']
})
export class UpdateProjectModalComponent implements OnInit {
  @Input() public project: Project;
  public projectForm: FormGroup;
  users: User[];
  teams: Team[];
  title: string = "Project Info";

  constructor(private teamService: TeamService, private projectService: ProjectService,
    private userService: UserService, public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.projectForm = new FormGroup({
      'name': new FormControl(this.project.name,
        [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      'description': new FormControl(this.project.name,
        [Validators.required, Validators.minLength(3)]),
      'createdAt': new FormControl(this.project.createdAt, Validators.required),
      'deadline': new FormControl(this.project.deadline, Validators.required),
      'authorId': new FormControl(this.project.authorId, Validators.required),
      'teamId': new FormControl(this.project.teamId, Validators.required)
    });

    this.getUsers();
    this.getTeams();
  }

  getUsers() {
    this.userService.getUsers()
      .subscribe(result => this.users = result,
        error => console.log(error));
  }

  getTeams() {
    this.teamService.getTeams()
      .subscribe(result => this.teams = result,
        error => console.log(error));
  }

  save() {
    this.projectService.updateProject(this.project)
      .subscribe(() => this.activeModal.close(this.project),
        () => this.activeModal.close());
  }
}