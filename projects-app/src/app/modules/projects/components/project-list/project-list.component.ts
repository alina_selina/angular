import { Component, OnInit, ViewChild } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { Project } from '../../../shared/models'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateProjectModalComponent } from '../update-project-modal/update-project-modal.component';
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  @ViewChild(SnackbarComponent, { static: false })
  private snackbarComponent: SnackbarComponent | undefined;
  title: string = "Projects";
  projects: Project[] = [];

  constructor(private router: Router, private projectService: ProjectService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects(): void {
    this.projectService.getProjects()
      .subscribe(result => this.projects = result,
        error=>{
          console.log(error);
          this.router.navigate(['/error'],{ state: { error: error.status, message: error.statusText }} );
        });
  }

  openModal(project: Project): void {
    const modalRef = this.modalService.open(UpdateProjectModalComponent);
    modalRef.componentInstance.project = project;
    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
      else {
        this.snackbarComponent?.showSnack("An error occured.");
      }
    });
  }

  delete(project: Project): void {
    if (confirm("Are you sure you want to delete this project?")) {
      this.projectService.deleteProject(project.id)
        .subscribe(() => this.getProjects(),
          error => {
            this.snackbarComponent?.showSnack("An error occured.");
            console.log(error);
          });
    }
  }
}
