import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectService } from '../../services/project.service';
import { Project } from '../../../shared/models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateProjectModalComponent } from '../update-project-modal/update-project-modal.component';
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
  @ViewChild(SnackbarComponent, { static: false })
  private snackbarComponent: SnackbarComponent | undefined;
  project: Project;
  title:string="Project Info";

  constructor(private route: ActivatedRoute, private service: ProjectService,
    private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.service.getProject(this.route.snapshot.params['id'])
    .subscribe(result => this.project = result,
      error=>{
        console.log(error);
        this.router.navigate(['/error'],{ state: { error: error.status, message: error.statusText }} );
      });
  }

  openModal() {
    const modalRef = this.modalService.open(UpdateProjectModalComponent);
    modalRef.componentInstance.project = this.project;
    modalRef.result.then((result) => {
      if (result) {
        this.router.navigate(['/projects'])
      } else {
        this.snackbarComponent?.showSnack("An error occured.");
      }
    });
  }

  delete() {
    if (confirm("Are you sure you want to delete this project?")) {
      this.service.deleteProject(this.project.id)
        .subscribe(() => this.router.navigate(['/projects']),
          error => {
            this.snackbarComponent?.showSnack("An error occured.");
            console.log(error);
          });
    }
  }
}
