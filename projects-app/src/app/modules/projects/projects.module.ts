import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import {SharedModule} from '../shared/shared.module';
import { ProjectService } from './services/project.service';
import { AppRoutingModule } from '../../app-routing.module';
import { UpdateProjectModalComponent } from './components/update-project-modal/update-project-modal.component';

@NgModule({
  declarations: [
    ProjectDetailsComponent,
    ProjectListComponent,
    UpdateProjectModalComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [ProjectService]
})
export class ProjectsModule { }
