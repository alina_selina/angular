import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../../shared/services';
import { Project } from '../../shared/models';

@Injectable({
  providedIn: 'root'
})

export class ProjectService {
  private readonly url: string = 'projects';

  constructor(private _apiService: ApiService) { }

  public getProjects(): Observable<Project[]> {
    return <Observable<Project[]>>this._apiService.get<Project>(this.url);
  }

  public getProject(id: number): Observable<Project> {
    return <Observable<Project>>this._apiService.get<Project>(`${this.url}/${id}`);
  }

  public deleteProject(id: number): Observable<Project> {
    return this._apiService.delete<Project>(`${this.url}/${id}`);
  }

  public updateProject(project: Project): Observable<Project> {
    return this._apiService.put<Project>(this.url, project);
  }
}
