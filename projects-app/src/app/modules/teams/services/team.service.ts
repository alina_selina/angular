import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../../shared/services';
import { Team, CreateTeam } from '../../shared/models';

@Injectable({
  providedIn: 'root'
})

export class TeamService {
  private readonly url: string = 'teams';

  constructor(private _apiService: ApiService) { }

  public getTeams(): Observable<Team[]> {
    return <Observable<Team[]>>this._apiService.get<Team>(this.url);
  }

  public getTeam(id: number): Observable<Team> {
    return <Observable<Team>>this._apiService.get<Team>(`${this.url}/${id}`);
  }

  public deleteTeam(id: number): Observable<Team> {
    return this._apiService.delete<Team>(`${this.url}/${id}`);
  }

  public updateTeam(team: Team): Observable<Team> {
    return this._apiService.put<Team>(this.url, team);
  }

  public createTeam(team:CreateTeam):Observable<Team>{
    return this._apiService.post<Team>(this.url, team);
  }
}
