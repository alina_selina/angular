import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TeamService } from '../../services/team.service';
import { CreateTeam } from '../../../shared/models';
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component';
import { FormComponent } from '../../../shared/guards/exit.guard';
import { Router } from '@angular/router';

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit, FormComponent {
  @ViewChild(SnackbarComponent, { static: false })
  private snackbarComponent: SnackbarComponent | undefined;
  public form: FormGroup;
  title: string = "Create Team";

  constructor(private router: Router, private teamService: TeamService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'name': new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      'createdAt': new FormControl(null),
    });
  }

  save() {
    const team: CreateTeam = {
      name: this.form.controls['name'].value,
      createdAt: this.form?.controls['createdAt']?.value,
    };

    this.teamService.createTeam(team)
      .subscribe(() => this.router.navigate(['/teams']),
        error => {
          this.snackbarComponent?.showSnack("An error occured.");
          console.log(error);
        });
  }
}
