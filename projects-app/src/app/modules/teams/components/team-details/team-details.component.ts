import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TeamService } from '../../services/team.service';
import { Team } from '../../../shared/models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateTeamModalComponent } from '../update-team-modal/update-team-modal.component';
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit {
  @ViewChild(SnackbarComponent, { static: false })
  private snackbarComponent: SnackbarComponent | undefined;
  team: Team;
  title:string="Team Info";

  constructor(private route: ActivatedRoute, private service: TeamService,
    private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.service.getTeam(this.route.snapshot.params['id'])
      .subscribe(result => this.team = result,
        error=>{
          console.log(error);
          this.router.navigate(['/error'],{ state: { error: error.status, message: error.statusText }} );
        });
  }

  openModal() {
    const modalRef = this.modalService.open(UpdateTeamModalComponent);
    modalRef.componentInstance.team = this.team;
    modalRef.result.then((result) => {
      if (result) {
        this.router.navigate(['/teams'])
      } else {
        this.snackbarComponent?.showSnack("An error occured.");
      }
    });
  }

  delete() {
    if (confirm("Are you sure you want to delete this team?")) {
      this.service.deleteTeam(this.team.id)
        .subscribe(() => this.router.navigate(['/teams']),
          error => {
            this.snackbarComponent?.showSnack("An error occured.");
            console.log(error);
          });
    }
  }
}
