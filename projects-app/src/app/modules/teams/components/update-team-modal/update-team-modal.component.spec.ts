import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTeamModalComponent } from './update-team-modal.component';

describe('UpdateTeamModalComponent', () => {
  let component: UpdateTeamModalComponent;
  let fixture: ComponentFixture<UpdateTeamModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateTeamModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTeamModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
