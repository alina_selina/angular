import { Component, Input, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TeamService } from '../../services/team.service';
import { Team } from '../../../shared/models';

@Component({
  selector: 'app-update-team-modal',
  templateUrl: './update-team-modal.component.html',
  styleUrls: ['./update-team-modal.component.css']
})
export class UpdateTeamModalComponent implements OnInit {
  @Input() public team:Team;
  public teamForm: FormGroup;
  title:string="Team Info";

  constructor(private teamService: TeamService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.teamForm = new FormGroup({
      'name': new FormControl(this.team.name, 
        [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      'createdAt': new FormControl(this.team.createdAt, [Validators.required]),
  });
  }

  save() {
    this.teamService.updateTeam(this.team)
    .subscribe(() => this.activeModal.close(this.team), 
    () => this.activeModal.close());
  }
}
