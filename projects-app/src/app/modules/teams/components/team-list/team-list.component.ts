import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TeamService } from '../../services/team.service';
import { Team } from '../../../shared/models';
import { UpdateTeamModalComponent } from '../update-team-modal/update-team-modal.component';
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
  @ViewChild(SnackbarComponent, { static: false })
  private snackbarComponent: SnackbarComponent | undefined;
  title: string = "Teams";
  teams: Team[] = [];

  constructor(private router: Router, private teamService: TeamService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.teamService.getTeams()
      .subscribe(result => this.teams = result,
        error => {
          console.log(error);
          this.router.navigate(['/error'], { state: { error: error.status, message: error.statusText } });
        });
  }

  redirect(route: number | string) {
    this.router.navigate(['/teams', route]);
  }

  openModal(team: Team) {
    const modalRef = this.modalService.open(UpdateTeamModalComponent);
    modalRef.componentInstance.team = team;
    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
      else {
        this.snackbarComponent?.showSnack("An error occured.");
      }
    });
  }

  delete(team: Team) {
    if (confirm("Are you sure you want to delete this team?")) {
      this.teamService.deleteTeam(team.id)
        .subscribe(() => this.ngOnInit(),
          error => {
            this.snackbarComponent?.showSnack("An error occured.");
            console.log(error);
          });
    }
  }
}
