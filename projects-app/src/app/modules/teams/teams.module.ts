import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import { TeamListComponent } from './components/team-list/team-list.component';
import { TeamDetailsComponent } from './components/team-details/team-details.component';
import { TeamService } from './services/team.service';
import { AppRoutingModule } from '../../app-routing.module';
import { UpdateTeamModalComponent } from './components/update-team-modal/update-team-modal.component';
import { TeamCreateComponent } from './components/team-create/team-create.component';

@NgModule({
  declarations: [
    TeamListComponent,
    TeamDetailsComponent,
    UpdateTeamModalComponent,
    TeamCreateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [TeamService]
})
export class TeamsModule { }
